from django.apps import AppConfig


class ComicappConfig(AppConfig):
    name = 'comicapp'
